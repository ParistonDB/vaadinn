package com.example.vaadindemo;

import com.example.vaadindemo.domain.Film;
import com.example.vaadindemo.domain.Person;
import com.example.vaadindemo.service.FilmManager;
import com.example.vaadindemo.service.PersonManager;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Title;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup.CommitException;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.validator.IntegerValidator;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Calendar;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.CustomLayout;
import com.vaadin.ui.DateField;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.client.ui.calendar.*;

@Theme("mytheme")
@Title("Vaadin Demo App")
public class VaadinAddMovie extends UI {
	private static final long serialVersionUID = 1L;

	private FilmManager filmManager = new FilmManager();

	private Film film = new Film("", "", null, "", "", "", false);
	private BeanItem<Film> filmItem = new BeanItem<Film>(film);

	private BeanItemContainer<Film> films = new BeanItemContainer<Film>(
			Film.class);

	enum Action {
		EDIT, ADD;
	}

	private class MyFormWindow extends Window {
		private static final long serialVersionUID = 1L;

		private Action action;

		public MyFormWindow(Action act) {
			this.action = act;

			setModal(true);
			center();
			
			switch (action) {
			case ADD:
				setCaption("Dodaj nowy film");
				break;
			case EDIT:
				setCaption("Edytuj film");
				break;
			default:
				break;
			}
			
			CustomLayout layout = new CustomLayout("mylayout");
			final FormLayout form = new FormLayout();
			final FieldGroup binder = new FieldGroup(filmItem);

			final Button saveBtn = new Button(" Dodaj film ");
			final Button cancelBtn = new Button(" Anuluj ");
			ComboBox menuKategorii = new ComboBox("Kategoria");
			menuKategorii.setInvalidAllowed(false);
			menuKategorii.setNullSelectionAllowed(false);
			menuKategorii.addItem("Horror");
			menuKategorii.addItem("Thriller");
			menuKategorii.addItem("Romans");
			menuKategorii.addItem("Fantastyczny");
			menuKategorii.addItem("Animowany");
			menuKategorii.addItem("Seans");
			menuKategorii.addItem("Dramat");
			menuKategorii.addItem("Obyczajowy");
			menuKategorii.addItem("Dokumentalny");
	        binder.bind(menuKategorii, "kategoria");
	        
	        DateField date = new DateField("Czas nadawania");
	        date.setDateFormat("dd/MM/YYYY");
	        binder.bind(date, "czasNadawania");
	        
	        TextArea opisek = new TextArea("Opis");
	        binder.bind(opisek, "opis");
	        
	        CheckBox plus18 = new CheckBox("Film dla osób pełnoletnich.");
	        binder.bind(plus18, "plus18");
	        
          form.addComponent(binder.buildAndBind("Tytul", "tytul"));
          binder.getField("tytul").addValidator(new StringLengthValidator(
        		    "Tytuł musi zawierać się w zakresie 1-50 znaków.",
        		    1, 50, true));
          form.addComponent(opisek);
          opisek.addValidator(new StringLengthValidator(
      		    "Opis nie może być dłuższy od 100 znaków i krótszy od 20.",
      		    20, 100, true));
          form.addComponent(binder.buildAndBind("Rezyser", "rezyser"));
          binder.getField("rezyser").addValidator(new StringLengthValidator(
        		    "Pole reżysera nie może być puste",
        		    5, 25, true));
          form.addComponent(date);
          form.addComponent(binder.buildAndBind("Kanal", "kanal"));
          binder.getField("kanal").addValidator(new StringLengthValidator(
        		    "Pole kanału nie może być puste.",
        		    1, 10, true));
			binder.setBuffered(true);

			binder.getField("tytul").setRequired(true);
			binder.getField("kanal").setRequired(true);
			
			VerticalLayout fvl = new VerticalLayout();
			fvl.setMargin(true);
			form.addComponent(menuKategorii);
			form.addComponent(plus18);
			fvl.addComponent(form);

			HorizontalLayout hl = new HorizontalLayout();
			hl.addComponent(saveBtn);
			hl.addComponent(cancelBtn);
			fvl.addComponent(hl);

			setContent(fvl);

			saveBtn.addClickListener(new ClickListener() {

				private static final long serialVersionUID = 1L;

				@Override
				public void buttonClick(ClickEvent event) {
					try {
						binder.commit();

						if (action == Action.ADD) {
							filmManager.addFilm(film);
						} else if (action == Action.EDIT) {
							filmManager.updateFilm(film);
						}

						films.removeAllItems();
						films.addAll(filmManager.findAll());
						close();
					} catch (CommitException e) {
						e.printStackTrace();
					}
				}
			});

			cancelBtn.addClickListener(new ClickListener() {

				private static final long serialVersionUID = 1L;

				@Override
				public void buttonClick(ClickEvent event) {
					binder.discard();
					close();
				}
			});
		}
	}

	@Override
	protected void init(VaadinRequest request) {
		Button addPersonFormBtn = new Button("Add ");
		Button deletePersonFormBtn = new Button("Delete");
		Button editPersonFormBtn = new Button("Edit");

		//VerticalLayout vl = new VerticalLayout();
		//setContent(vl);

		addPersonFormBtn.addClickListener(new ClickListener() {

			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				addWindow(new MyFormWindow(Action.ADD));
			}
		});

		editPersonFormBtn.addClickListener(new ClickListener() {

			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				addWindow(new MyFormWindow(Action.EDIT));
			}
		});

		deletePersonFormBtn.addClickListener(new ClickListener() {

			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				if (!film.getTytul().isEmpty()) {
					filmManager.delete(film);
					films.removeAllItems();
					films.addAll(filmManager.findAll());
				}
			}
		});

		CustomLayout CL = new CustomLayout("mylayout");		
		setContent(CL);
		
		Label header = new Label();
		header.addStyleName("header");
		CL.addComponent(header, "header");
		
		Label logo_db = new Label();
		logo_db.addStyleName("logo_db");
		CL.addComponent(logo_db, "header");
		
		Label left_menu = new Label();
		left_menu.addStyleName("left_menu");
		CL.addComponent(left_menu, "left_menu");
		
		Label right_menu = new Label();
		right_menu.addStyleName("right_menu");
		CL.addComponent(right_menu, "right_menu");
		
		HorizontalLayout hl = new HorizontalLayout();
		hl.addStyleName("buttons");
		hl.addComponent(addPersonFormBtn);
		hl.addComponent(editPersonFormBtn);
		hl.addComponent(deletePersonFormBtn);

		final Table filmsTable = new Table("", films);
		filmsTable.setColumnHeader("Tytuł", "tytul");
		filmsTable.setColumnHeader("Opis", "opis");
		filmsTable.setColumnHeader("Data", "czasNadawania");
		filmsTable.setColumnHeader("Reżyser", "rezyser");
		filmsTable.setColumnHeader("Kanał", "kanal");
		filmsTable.setColumnHeader("Kategoria", "kategoria");
		filmsTable.setColumnHeader("18+", "plus18");
		filmsTable.setSelectable(true);
		filmsTable.setImmediate(true);

		filmsTable.addValueChangeListener(new Property.ValueChangeListener() {

			private static final long serialVersionUID = 1L;

			@Override
			public void valueChange(ValueChangeEvent event) {

				Film selectedFilm = (Film) filmsTable.getValue();
				if (selectedFilm == null) {
					film.setTytul("");
					film.setOpis("");
					film.setCzasNadawania(null);
					film.setRezyser("");
					film.setKanal("");
					film.setKategoria("");
					film.setPlus18(false);
					film.setId(null);
				} else {
					film.setTytul(selectedFilm.getTytul());
					film.setOpis(selectedFilm.getOpis());
					film.setCzasNadawania(selectedFilm.getCzasNadawania());
					film.setRezyser(selectedFilm.getRezyser());
					film.setKanal(selectedFilm.getKanal());
					film.setKategoria(selectedFilm.getKategoria());
					film.setPlus18(selectedFilm.isPlus18());
					film.setId(selectedFilm.getId());
				}
			}
		});

		CL.addComponent(hl, "left_menu");
		CL.addComponent(filmsTable, "right_menu");
		
		HorizontalLayout horizontalLayout = new HorizontalLayout();
		Label label = new Label();
		horizontalLayout.addComponent(label);
		label.setValue(UI.getCurrent().toString());
		
		CL.addComponent(horizontalLayout);
	}

}

