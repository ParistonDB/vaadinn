package com.example.vaadindemo;



import com.example.vaadindemo.domain.Film;
import com.example.vaadindemo.domain.Person;
import com.example.vaadindemo.domain.User;
import com.example.vaadindemo.service.FilmManager;
import com.example.vaadindemo.service.PersonManager;
import com.example.vaadindemo.service.UserManager;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Title;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup.CommitException;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.validator.IntegerValidator;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.server.ExternalResource;
import com.vaadin.server.Page;
import com.vaadin.server.Resource;
import com.vaadin.server.ThemeResource;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Calendar;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.CustomLayout;
import com.vaadin.ui.DateField;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.Link;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.TabSheet.Tab;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.client.ui.calendar.*;
import com.vaadin.event.LayoutEvents.LayoutClickEvent;
import com.vaadin.event.LayoutEvents.LayoutClickListener;
@Theme("mytheme")
@Title("Vaadin Demo App")

public class VaadinTable extends UI {
	private static final long serialVersionUID = 1L;
	final TabSheet tabSheet = new TabSheet();
	final CustomLayout CL = new CustomLayout("tableLayout");
	VerticalLayout right_layout = new VerticalLayout();
	
	private FilmManager filmManager = new FilmManager();

	private Film film = new Film("Wladca Pierscieni",
			"Film o hobbicie przypadkowym calkowicie xD", null, "Peter Jackson",
			"Polsat", "", false);
	private BeanItem<Film> filmItem = new BeanItem<Film>(film);

	private BeanItemContainer<Film> films = new BeanItemContainer<Film>(
			Film.class);
	
	private UserManager userManager = new UserManager();
	private User user = new User("", "");
	private BeanItem<User> userItem = new BeanItem<User>(user);
	private BeanItemContainer<User> users = new BeanItemContainer<User>(
			User.class);
	
	enum Action {
		EDIT, ADD;
	}

	private class MyFormWindow extends Window {
		
		private static final long serialVersionUID = 1L;

		private Action action;

		public MyFormWindow(Action act) {
			this.action = act;

			setModal(true);
			center();
			
			switch (action) {
			case ADD:
				setCaption("Dodaj nowy film");
				break;
			case EDIT:
				setCaption("Edytuj film");
				break;
			default:
				break;
			}
			
			final FormLayout form = new FormLayout();
			
			final FieldGroup binder = new FieldGroup(filmItem);
			final Button saveBtn = new Button(" Dodaj film ");
			final Button cancelBtn = new Button(" Anuluj ");
			ComboBox menuKategorii = new ComboBox("Kategoria");
			menuKategorii.setInvalidAllowed(false);
			menuKategorii.setNullSelectionAllowed(false);
			menuKategorii.addItem("Horror");
			menuKategorii.addItem("Thriller");
			menuKategorii.addItem("Romans");
			menuKategorii.addItem("Fantastyczny");
			menuKategorii.addItem("Animowany");
			menuKategorii.addItem("Seans");
			menuKategorii.addItem("Dramat");
			menuKategorii.addItem("Obyczajowy");
			menuKategorii.addItem("Dokumentalny");
	        binder.bind(menuKategorii, "kategoria");
	        
	        DateField date = new DateField("Czas nadawania");
	        date.setDateFormat("dd/MM/YYYY");
	        binder.bind(date, "czasNadawania");
	        
	        TextArea opisek = new TextArea("Opis");
	        binder.bind(opisek, "opis");
	        
	        CheckBox plus18 = new CheckBox("Film dla osób pełnoletnich.");
	        binder.bind(plus18, "plus18");
	        
          form.addComponent(binder.buildAndBind("Tytul", "tytul"));
          binder.getField("tytul").addValidator(new StringLengthValidator(
        		    "Tytuł musi zawierać się w zakresie 1-50 znaków.",
        		    1, 50, true));
          form.addComponent(opisek);
          opisek.addValidator(new StringLengthValidator(
      		    "Opis nie może być krótszy od 20 znaków.",
      		    20, 1000000, true));
          form.addComponent(binder.buildAndBind("Rezyser", "rezyser"));
          binder.getField("rezyser").addValidator(new StringLengthValidator(
        		    "Pole reżysera nie może być puste",
        		    5, 25, true));
          form.addComponent(date);
          form.addComponent(binder.buildAndBind("Kanal", "kanal"));
          binder.getField("kanal").addValidator(new StringLengthValidator(
        		    "Pole kanału nie może być puste.",
        		    1, 10, true));
			binder.setBuffered(true);

			binder.getField("tytul").setRequired(true);
			binder.getField("kanal").setRequired(true);
			
			VerticalLayout fvl = new VerticalLayout();
			fvl.setMargin(true);
			form.addComponent(menuKategorii);
			form.addComponent(plus18);
			fvl.addComponent(form);

			HorizontalLayout hl = new HorizontalLayout();
			hl.addComponent(saveBtn);
			hl.addComponent(cancelBtn);
			fvl.addComponent(hl);

			setContent(fvl);

			saveBtn.addClickListener(new ClickListener() {

				private static final long serialVersionUID = 1L;

				@Override
				public void buttonClick(ClickEvent event) {
					try {
						binder.commit();

						if (action == Action.ADD) {
							filmManager.addFilm(film);																	
						} else if (action == Action.EDIT) {
							filmManager.updateFilm(film);
						}

						films.removeAllItems();
						films.addAll(filmManager.findAll());
						close();
					} catch (CommitException e) {
						e.printStackTrace();
					}
				}
			});

			cancelBtn.addClickListener(new ClickListener() {

				private static final long serialVersionUID = 1L;

				@Override
				public void buttonClick(ClickEvent event) {
					binder.discard();
					close();
				}
			});
		}
	}

	@Override
	protected void init(VaadinRequest request) {
		CL.addComponent(right_layout, "right_menu");
		Button addPersonFormBtn = new Button("Dodaj nową pozycję");
		Button deletePersonFormBtn = new Button("Usuń film");
		Button editPersonFormBtn = new Button("Dokonaj edycji");
		setContent(CL);
		right_layout.setWidth("100%");
		
		addPersonFormBtn.addClickListener(new ClickListener() {

			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				addWindow(new MyFormWindow(Action.ADD));
			}
		});

		editPersonFormBtn.addClickListener(new ClickListener() {

			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				addWindow(new MyFormWindow(Action.EDIT));
			}
		});

		deletePersonFormBtn.addClickListener(new ClickListener() {

			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				
				if (!film.getTytul().isEmpty()) {
					filmManager.delete(film);
					films.removeAllItems();
					films.addAll(filmManager.findAll());
				}
			}
		});
		
		Label header = new Label();
		header.addStyleName("header");
		CL.addComponent(header, "header");
		
		HorizontalLayout szukanie = new HorizontalLayout();
		szukanie.setStyleName("szukanie");
		
		TextField szukajka = new TextField();
		szukajka.setInputPrompt("Szukaj...");
		szukajka.setStyleName("szukajka");

		Button szukaj = new Button();
		szukaj.setStyleName("szukaj_button");
		
		szukanie.addComponent(szukajka);
		szukanie.addComponent(szukaj);
		
		CL.addComponent(szukanie, "header");
		VerticalLayout logo_db = new VerticalLayout();
		logo_db.addStyleName("logo_db");
		
		Label left_menu = new Label();
		left_menu.addStyleName("left_menu");
		CL.addComponent(left_menu, "left_menu");
		
		CL.addComponent(logo_db, "linkowanko");
		
		VerticalLayout hl = new VerticalLayout();		
		addPersonFormBtn.addStyleName("btn_");
		editPersonFormBtn.addStyleName("btn_");
		deletePersonFormBtn.addStyleName("btn_");
		
		final FormLayout form2 = new FormLayout();
		final FieldGroup binder2 = new FieldGroup(userItem);
		TextField login_login = new TextField("Login");
		binder2.bind(login_login, "login");
		login_login.addStyleName("loginMain");
		form2.addComponent(login_login);
		
		PasswordField login_haslo = new PasswordField("Hasło");
		binder2.bind(login_haslo, "haslo");
		login_haslo.addStyleName("loginMain");
		form2.addComponent(login_haslo);
		
		Button zaloguj = new Button("Zaloguj");
		zaloguj.addStyleName("btn_2");
		form2.addComponent(zaloguj);
		
		Label menuLogowania = new Label("Logowanie do systemu");
		menuLogowania.addStyleName("menuFilmu");
		hl.addComponent(menuLogowania);		
		hl.addComponent(form2);
		
		Label menuFilmu = new Label("Twoje menu");
		menuFilmu.addStyleName("menuFilmu");
		hl.addComponent(menuFilmu);		
		hl.addComponent(addPersonFormBtn);
		hl.addComponent(editPersonFormBtn);
		hl.addComponent(deletePersonFormBtn);
		
		Label nawigacja = new Label("Nawigacja");
		nawigacja.addStyleName("menuFilmu");
		hl.addComponent(nawigacja);			
		
		Button stronaGlowna = new Button("Strona główna");
		stronaGlowna.addStyleName("btn_3");
		hl.addComponent(stronaGlowna);

		stronaGlowna.addClickListener(new ClickListener() {

			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				getUI().getPage().setLocation("http://www.localhost:8080/vaadindemo");
			}
		});
		
		Button przegladajFilmy = new Button("Przeglądaj Filmy");
		przegladajFilmy.addStyleName("btn_3");
		hl.addComponent(przegladajFilmy);

		przegladajFilmy.addClickListener(new ClickListener() {

			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				getUI().getPage().setLocation("http://www.localhost:8080/vaadindemo/filmy");
			}
		});
		
		Button oNas = new Button("Kilka słów o projekcie");
		oNas.addStyleName("btn_3");
		hl.addComponent(oNas);

		oNas.addClickListener(new ClickListener() {

			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				getUI().getPage().setLocation("http://www.localhost:8080/vaadindemo/onas");
			}
		});
		
		final Table filmsTable = new Table("", films);
		filmsTable.setColumnHeader("Tytuł", "tytul");
		filmsTable.setColumnHeader("Opis", "opis");
		filmsTable.setColumnHeader("Data", "czasNadawania");
		filmsTable.setColumnHeader("Reżyser", "rezyser");
		filmsTable.setColumnHeader("Kanał", "kanal");
		filmsTable.setColumnHeader("Kategoria", "kategoria");
		filmsTable.setColumnHeader("18+", "plus18");
		filmsTable.setSelectable(true);
		filmsTable.setImmediate(true);
		
		filmsTable.addValueChangeListener(new Property.ValueChangeListener() {

			private static final long serialVersionUID = 1L;

			@Override
			public void valueChange(ValueChangeEvent event) {

				Film selectedFilm = (Film) filmsTable.getValue();
				if (selectedFilm == null) {
					film.setTytul("");
					film.setOpis("");
					film.setCzasNadawania(null);
					film.setRezyser("");
					film.setKanal("");
					film.setKategoria("");
					film.setPlus18(false);
					film.setId(null);
				} else {
					film.setTytul(selectedFilm.getTytul());
					film.setOpis(selectedFilm.getOpis());
					film.setCzasNadawania(selectedFilm.getCzasNadawania());
					film.setRezyser(selectedFilm.getRezyser());
					film.setKanal(selectedFilm.getKanal());
					film.setKategoria(selectedFilm.getKategoria());
					film.setPlus18(selectedFilm.isPlus18());
					film.setId(selectedFilm.getId());
				}
			}
		});
		
		filmsTable.setStyleName("tabela_filmow");
		CL.addComponent(hl, "left_menu");
		CL.addComponent(filmsTable, "right_menu");
	}

}
