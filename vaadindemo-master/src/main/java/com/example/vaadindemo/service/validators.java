package com.example.vaadindemo.service;

import com.vaadin.data.Validator;
import com.vaadin.data.Validator.InvalidValueException;

public class Validators {
	
    public Validator validuj_opis = new Validator() {

		@Override
		public void validate(Object value) throws InvalidValueException {
			String opis = (String) value;
			if(opis.length() <= 10 || opis.length() >=200) {
				throw new InvalidValueException("Za mało znaków! Od 10 do 200");
			}
		}  
    };
    
    public Validator validuj_rezysera = new Validator() {

		@Override
		public void validate(Object value) throws InvalidValueException {
			String rezyser = (String) value;
			if(rezyser.length() <= 0) {
				throw new InvalidValueException("Wypełnij pole!");
			}
		}  
    };

    public Validator validuj_tytul = new Validator() {

		@Override
		public void validate(Object value) throws InvalidValueException {
			String tytul = (String) value;
			if(tytul.length() <= 0 || tytul.length() >= 25) {
				throw new InvalidValueException("Za mało znaków! Od 0 do 25");
			}
		}  
    };
    
    public Validator validuj_kanal = new Validator() {

		@Override
		public void validate(Object value) throws InvalidValueException {
			String kanal = (String) value;
			if(kanal.length() <= 0 || kanal.length() >= 15) {
				throw new InvalidValueException("Za mało znaków! Od 0 do 15");
			}
		}  
    };
    
    public Validator validuj_kategorie = new Validator() {

		@Override
		public void validate(Object value) throws InvalidValueException {
			String kategoria = (String) value;
			if(kategoria.length() <= 0) {
				throw new InvalidValueException("Wybierz kategorię!");
			}
		}  
    };
}
