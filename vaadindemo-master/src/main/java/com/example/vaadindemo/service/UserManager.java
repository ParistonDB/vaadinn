package com.example.vaadindemo.service;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.example.vaadindemo.domain.User;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinService;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;

public class UserManager extends UI {
	
	private static final long serialVersionUID = 1L;
	
	private List<User> db = new ArrayList<User>();
	
	public void addUser(User user) {
		User u = new User(user.getLogin(), user.getHaslo());
		u.setId(UUID.randomUUID());
		db.add(u);
	}
	
	public void deleteUser(User user) {
	}
	
	public void logIn(User user) {
		for(User u: db) {
			if(user.getLogin().equals(u.getLogin())) {
				if(user.getHaslo().equals(u.getHaslo())) {
					//getSession().setAttribute("user", user);
					getUI().getPage().setLocation("http://www.localhost:8080/vaadindemo");
				}
			}
		}	
	}

	@Override
	protected void init(VaadinRequest request) {
		// TODO Auto-generated method stub
		
	}
}
