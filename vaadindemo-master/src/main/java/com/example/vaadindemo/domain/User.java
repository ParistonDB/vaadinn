package com.example.vaadindemo.domain;

import java.util.UUID;

public class User {
	private UUID id;
	
	private String login;
	private String haslo;
	
	public User(String login, String haslo) {
		super();
		this.login = login;
		this.haslo = haslo;
	}
	
	public UUID getId() {
		return id;
	}
	public void setId(UUID id) {
		this.id = id;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getHaslo() {
		return haslo;
	}
	public void setHaslo(String haslo) {
		this.haslo = haslo;
	}
	
	
}
