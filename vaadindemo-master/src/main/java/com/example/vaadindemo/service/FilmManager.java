package com.example.vaadindemo.service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.example.vaadindemo.domain.Film;

public class FilmManager {
	
	private List<Film> db = new ArrayList<Film>();
	
	public void addFilm(Film film){
		Film f = new Film(film.getTytul(), film.getOpis(), film.getCzasNadawania(), 
				film.getRezyser(), film.getKanal(), film.getKategoria(), film.isPlus18());
		f.setId(UUID.randomUUID());
		db.add(f);
	}
	
	public List<Film> findAll(){
		return db;
	}

	public void delete(Film film) {
		
		Film toRemove = null;
		for (Film f: db) {
			if (f.getId().compareTo(film.getId()) == 0){
				toRemove = f;
				break;
			}
		}
		db.remove(toRemove);		
	}

	public void updateFilm(Film film) {
		Film f2 = new Film(film.getTytul(), film.getOpis(), film.getCzasNadawania(), 
				film.getRezyser(), film.getKanal(), film.getKategoria(), film.isPlus18());
		
		Film toEdit = null;
		for (Film f: db) {
			if (f.getId().compareTo(film.getId()) == 0){
				toEdit = f;
				break;
			}
		}
		int indeks = db.indexOf(toEdit);
		f2.setId(toEdit.getId());
		db.set(indeks, f2);	
	}
}
