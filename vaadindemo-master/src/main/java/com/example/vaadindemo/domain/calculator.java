package com.example.vaadindemo.domain;
 
 
import com.vaadin.annotations.Title;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
 
@Title("Vaadin - Kalkulator")
public class VaadinApp extends UI implements ClickListener {
 
private static final long serialVersionID= 1L;
 
        GridLayout hl = new GridLayout(4, 4);
        Button button;
        double obecnyWynik = 0.0;
        double koncowyWynik = 0.0;
        private final Label display = new Label("0.0");
        private char ostatniaOperacja = 'C';
       
        @Override
        protected void init(VaadinRequest request) {
                hl.addComponent(display, 0, 0, 3, 0);
                String operacje[] = new String[] { "7", "8", "9", "/", "4", "5", "6",
                "*", "1", "2", "3", "-", "0", "=", "C", "+" };
               
                for(String wybor : operacje){
                        button = new Button(wybor);
                        button.addClickListener(this);
                        hl.addComponent(button);
                }
 
                setContent(hl);
        }
       
        public void buttonClick(ClickEvent event) {
                Button button = event.getButton();
                char wybranaOperacja = button.getCaption().charAt(0);
                double wartosc = policz(wybranaOperacja);
                display.setValue(Double.toString(wartosc));
        }
       
        private double policz(char operacja) {
        if ('0' <= operacja && operacja <= '9') {
            obecnyWynik = obecnyWynik * 10
                    + Double.parseDouble("" + operacja);
            return obecnyWynik;
        }
       
                switch(ostatniaOperacja) {
                        case '-':
                                koncowyWynik-=obecnyWynik;
                                break;
                        case '+':
                                koncowyWynik+=obecnyWynik;
                                break;
                        case '/':
                                koncowyWynik/=obecnyWynik;
                                break;
                        case '*':
                                koncowyWynik*=obecnyWynik;
                                break;
                        case 'C':
                                koncowyWynik = obecnyWynik;
                                break;
                }
               
                ostatniaOperacja = operacja;
                obecnyWynik = 0.0;
                if(operacja == 'C') {
                        koncowyWynik = 0.0;
                }
                return koncowyWynik;
        }
}