package com.example.vaadindemo.domain;


import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.UUID;

import javax.validation.constraints.Size;

public class Film {
    
    private UUID id;
    
    private String tytul;
    private String opis;
    private String rezyser;
    private Date czasNadawania;
    private String kanal;
    private String kategoria;
    private boolean plus18;
    
    public Film(String tytul, String opis, Date czasNadawania, String rezyser, String kanal, String kategoria, boolean plus18) {
            super();
            this.tytul = tytul;
            this.opis = opis;
            this.czasNadawania = czasNadawania;
            this.rezyser = rezyser;
            this.kanal = kanal;
            this.kategoria = kategoria;
            this.plus18 = plus18;
    }
   
    public boolean isPlus18() {
		return plus18;
	}

	public void setPlus18(boolean plus18) {
		this.plus18 = plus18;
	}

	public String getKanal() {
		return kanal;
	}

	public String getKategoria() {
		return kategoria;
	}

	public void setKategoria(String kategoria) {
		this.kategoria = kategoria;
	}

	public void setKanal(String kanal) {
		this.kanal = kanal;
	}

	public Date getCzasNadawania() {
		return czasNadawania;
	}
	
	public String formatCzasu(Date czas) {
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		String czasNadawania_format = df.format(czas);
		
		return czasNadawania_format;
	}
	
	public void setCzasNadawania(Date czasNadawania) {
		this.czasNadawania = czasNadawania;
	}

	public UUID getId() {
            return id;
    }
    public void setId(UUID id) {
            this.id = id;
    }
    public String getTytul() {
            return tytul;
    }
    public void setTytul(String tytul) {
            this.tytul = tytul;
    }
    public String getOpis() {
            return opis;
    }
    public void setOpis(String opis) {
            this.opis = opis;
    }
    public String getRezyser() {
            return rezyser;
    }
    public void setRezyser(String rezyser) {
            this.rezyser = rezyser;
    }
   
   
}

