package com.example.vaadindemo;

import com.vaadin.data.Validator;

public class Validators {
	
	public Validator valid_tytul = new Validator() {
		@Override
		public void validate(Object value) throws InvalidValueException {
			String tytul = (String) value;
			if(tytul.length() <= 0 || tytul.length() > 50) {
				throw new InvalidValueException("Maksymalna ilość znaków dla tytułu to 0-50.");
			}
		}	
	};

	public Validator valid_opis = new Validator() {
		@Override
		public void validate(Object value) throws InvalidValueException {
			String tytul = (String) value;
			if(tytul.length() < 20 || tytul.length() > 5000) {
				throw new InvalidValueException("Maksymalna ilość znaków dla opisu to 20-500.");
			}
		}	
	};
	
	public Validator valid_rezyser = new Validator() {
		@Override
		public void validate(Object value) throws InvalidValueException {
			String tytul = (String) value;
			if(tytul.length() <= 0) {
				throw new InvalidValueException("Uzupełnij pole.");
			}
		}	
	};
	
	public Validator valid_kanal = new Validator() {
		@Override
		public void validate(Object value) throws InvalidValueException {
			String tytul = (String) value;
			if(tytul.length() <= 0) {
				throw new InvalidValueException("Uzupełnij pole.");
			}
		}	
	};
	
	public Validator valid_kategoria = new Validator() {
		@Override
		public void validate(Object value) throws InvalidValueException {
			String tytul = (String) value;
			if(tytul.length() <= 0) {
				throw new InvalidValueException("Wybierz kategorię.");
			}
		}	
	};
}
