package com.example.vaadindemo;



import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.EventObject;
import com.example.vaadindemo.Validators;
import com.example.vaadindemo.domain.Film;
import com.example.vaadindemo.domain.Person;
import com.example.vaadindemo.domain.User;
import com.example.vaadindemo.service.FilmManager;
import com.example.vaadindemo.service.PersonManager;
import com.example.vaadindemo.service.UserManager;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Title;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup.CommitException;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.validator.IntegerValidator;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.server.ExternalResource;
import com.vaadin.server.Page;
import com.vaadin.server.Resource;
import com.vaadin.server.ThemeResource;
import com.vaadin.server.VaadinRequest;
import com.vaadin.shared.ui.datefield.Resolution;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Calendar;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.CustomLayout;
import com.vaadin.ui.DateField;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.Link;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.RichTextArea;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.TabSheet.Tab;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.client.ui.calendar.*;
import com.vaadin.event.LayoutEvents.LayoutClickEvent;
import com.vaadin.event.LayoutEvents.LayoutClickListener;
@Theme("mytheme")
@Title("Vaadin Demo App")
public class VaadinApp extends UI {
	private static final long serialVersionUID = 1L;
	final TabSheet tabSheet = new TabSheet();
	final CustomLayout CL = new CustomLayout("mylayout");
	VerticalLayout right_layout = new VerticalLayout();
	
	private FilmManager filmManager = new FilmManager();

	private Film film = new Film("Wladca Pierscieni",
			"Wszystkie trzy części opowiadają o Wojnie o Pierścień z mitologii Śródziemia oraz o...", 
			null, "Peter Jackson",
			"Polsat", "", false);
	private BeanItem<Film> filmItem = new BeanItem<Film>(film);

	private BeanItemContainer<Film> films = new BeanItemContainer<Film>(
			Film.class);
	
	private UserManager userManager = new UserManager();
	private User user = new User("test", "test");

	private BeanItem<User> userItem = new BeanItem<User>(user);
	private BeanItemContainer<User> users = new BeanItemContainer<User>(
			User.class);
	Validators val = new Validators();
	
	enum Action {
		EDIT, ADD;
	}

	private class MyFormWindow extends Window {
		
		private static final long serialVersionUID = 1L;
		
		private Action action;

		public MyFormWindow(Action act) {
			
			this.action = act;

			setModal(true);
			center();
			
			switch (action) {
			case ADD:
				setCaption("Dodaj nowy film");
				break;
			case EDIT:
				setCaption("Edytuj film");
				break;
			default:
				break;
			}
			
			final FormLayout form = new FormLayout();
			
			final FieldGroup binder = new FieldGroup(filmItem);
			final Button saveBtn = new Button(" Dodaj film ");
			final Button cancelBtn = new Button(" Anuluj ");
			ComboBox menuKategorii = new ComboBox("Kategoria");
			menuKategorii.setInvalidAllowed(false);
			menuKategorii.setNullSelectionAllowed(false);
			menuKategorii.addItem("Horror");
			menuKategorii.addItem("Thriller");
			menuKategorii.addItem("Romans");
			menuKategorii.addItem("Fantastyczny");
			menuKategorii.addItem("Animowany");
			menuKategorii.addItem("Seans");
			menuKategorii.addItem("Dramat");
			menuKategorii.addItem("Obyczajowy");
			menuKategorii.addItem("Dokumentalny");
	        binder.bind(menuKategorii, "kategoria");
	        menuKategorii.addValidator(val.valid_kategoria);
	        
	        DateField date = new DateField("Czas nadawania");
	        date.setResolution(Resolution.SECOND);
	        binder.bind(date, "czasNadawania");
	        
	        RichTextArea opisek = new RichTextArea("Opis");
	        binder.bind(opisek, "opis");
	        
	        CheckBox plus18 = new CheckBox("Film dla osób pełnoletnich.");
	        binder.bind(plus18, "plus18");
	        
          form.addComponent(binder.buildAndBind("Tytul", "tytul"));
          binder.getField("tytul").addValidator(val.valid_tytul);
          
          form.addComponent(opisek);
          opisek.addValidator(val.valid_opis);
          
          form.addComponent(binder.buildAndBind("Rezyser", "rezyser"));
          binder.getField("rezyser").addValidator(val.valid_rezyser);
          
          form.addComponent(date);
          
          form.addComponent(binder.buildAndBind("Kanal", "kanal"));
          binder.getField("kanal").addValidator(val.valid_kanal);
			binder.setBuffered(true);

			binder.getField("tytul").setRequired(true);
			binder.getField("kanal").setRequired(true);
			binder.getField("rezyser").setRequired(true);
			binder.getField("czasNadawania").setRequired(true);
			
			VerticalLayout fvl = new VerticalLayout();
			fvl.setMargin(true);
			form.addComponent(menuKategorii);
			form.addComponent(plus18);
			fvl.addComponent(form);

			HorizontalLayout hl = new HorizontalLayout();
			hl.addComponent(saveBtn);
			hl.addComponent(cancelBtn);
			fvl.addComponent(hl);

			setContent(fvl);
			final Table filmsTable2 = new Table("", films);
			filmsTable2.setColumnHeader("Tytuł", "tytul");
			filmsTable2.setColumnHeader("Opis", "opis");
			filmsTable2.setColumnHeader("Data", "czasNadawania");
			filmsTable2.setColumnHeader("Reżyser", "rezyser");
			filmsTable2.setColumnHeader("Kanał", "kanal");
			filmsTable2.setColumnHeader("Kategoria", "kategoria");
			filmsTable2.setColumnHeader("18+", "plus18");
			filmsTable2.setSelectable(true);
			filmsTable2.setImmediate(true);
			
			filmsTable2.addValueChangeListener(new Property.ValueChangeListener() {
				
				private static final long serialVersionUID = 1L;
				
				@Override
				public void valueChange(ValueChangeEvent event) {

					Film selectedFilm = (Film) filmsTable2.getValue();
					if (selectedFilm == null) {
						film.setTytul("");
						film.setOpis("");
						film.setCzasNadawania(null);
						film.setRezyser("");
						film.setKanal("");
						film.setKategoria("");
						film.setPlus18(false);
						film.setId(null);
					} else {
						film.setTytul(selectedFilm.getTytul());
						film.setOpis(selectedFilm.getOpis());
						film.setCzasNadawania(selectedFilm.getCzasNadawania());
						film.setRezyser(selectedFilm.getRezyser());
						film.setKanal(selectedFilm.getKanal());
						film.setKategoria(selectedFilm.getKategoria());
						film.setPlus18(selectedFilm.isPlus18());
						film.setId(selectedFilm.getId());
					}
				}
			});	
			saveBtn.addClickListener(new ClickListener() {

				private static final long serialVersionUID = 1L;

				@Override
				public void buttonClick(ClickEvent event) {
					try {
						binder.commit();

						if (action == Action.ADD) {				
							filmManager.addFilm(film);
						
							
							final VerticalLayout vl5 = new VerticalLayout();
							final HorizontalLayout glowny = new HorizontalLayout();
							final HorizontalLayout dane = new HorizontalLayout();
							
							glowny.addStyleName("film_podklad");

							final CheckBox doedycji = new CheckBox();
							doedycji.setData(film);
							
							final CheckBox dousuniecia = new CheckBox();
							dousuniecia.setData(film);
							
							glowny.addComponent(dousuniecia);
							
							Label film_ikona = new Label();
							film_ikona.addStyleName("film_ikona");
							glowny.addComponent(film_ikona);
							
							
							vl5.setWidth("900px");

							Label tytul = new Label(film.getTytul());
							Label opis = new Label(film.getOpis(), ContentMode.HTML);
							Label rezyser = new Label(film.getRezyser());
							Label plus18 = new Label();
							Label kanal = new Label(film.getKanal());
							Label kategoria = new Label(film.getKategoria());							
							Label czasNadawania = new Label(film.formatCzasu(film.getCzasNadawania()));
							
							tytul.setStyleName("film_tytul");
							opis.setStyleName("film_opis");
							rezyser.setStyleName("film_rezyser");
							plus18.setStyleName("plus18");	
							kanal.setStyleName("film_kanal");
							kategoria.setStyleName("film_kategoria");
							czasNadawania.setStyleName("film_kategoria");
							
							vl5.addComponent(tytul);
							vl5.addComponent(opis);
							dane.addComponent(rezyser);
							dane.addComponent(kanal);
							dane.addComponent(kategoria);
							dane.addComponent(czasNadawania);
							vl5.addComponent(dane);
							
							//vl5.addComponent(filmsTable2);
							
							if(film.isPlus18() == true)
								vl5.addComponent(plus18);
							
							glowny.addComponent(vl5);

							dousuniecia.addValueChangeListener(new Property.ValueChangeListener() {
								
								private static final long serialVersionUID = 1L;
								
								@Override
								public void valueChange(ValueChangeEvent event) {
									right_layout.removeComponent(glowny);
									filmsTable2.select(film);							
									Film sele = (Film) filmsTable2.getValue();
									film.setId(sele.getId());
									
									filmManager.delete(film);
									films.removeAllItems();
									films.addAll(filmManager.findAll());
								}								
							});
							
							right_layout.addComponent(glowny);
														
							
						} else if (action == Action.EDIT) {
							filmManager.updateFilm(film);
						}

						films.removeAllItems();
						films.addAll(filmManager.findAll());
						close();
					} catch (CommitException e) {
						e.printStackTrace();
					}
				}
			});

			cancelBtn.addClickListener(new ClickListener() {

				private static final long serialVersionUID = 1L;

				@Override
				public void buttonClick(ClickEvent event) {
					binder.discard();
					close();
				}
			});
		}
	}

	@Override
	protected void init(VaadinRequest request) {
		CL.addComponent(right_layout, "right_menu");
		CL.setWidth("100%");
		Button addPersonFormBtn = new Button("Dodaj nową pozycję");
		Button deletePersonFormBtn = new Button("Usuń film");
		Button editPersonFormBtn = new Button("Dokonaj edycji");
		setContent(CL);
		right_layout.setWidth("100%");
		
		addPersonFormBtn.addClickListener(new ClickListener() {

			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				addWindow(new MyFormWindow(Action.ADD));
			}
		});

		editPersonFormBtn.addClickListener(new ClickListener() {

			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				addWindow(new MyFormWindow(Action.EDIT));
			}
		});

		deletePersonFormBtn.addClickListener(new ClickListener() {

			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				
				if (!film.getTytul().isEmpty()) {
					filmManager.delete(film);
					films.removeAllItems();
					films.addAll(filmManager.findAll());
				}
			}
		});
		
		Label header = new Label();
		header.addStyleName("header");
		CL.addComponent(header, "header");
		
		HorizontalLayout szukanie = new HorizontalLayout();
		szukanie.setStyleName("szukanie");
		
		TextField szukajka = new TextField();
		szukajka.setInputPrompt("Szukaj...");
		szukajka.setStyleName("szukajka");

		Button szukaj = new Button();
		szukaj.setStyleName("szukaj_button");
		
		szukanie.addComponent(szukajka);
		szukanie.addComponent(szukaj);
		
		CL.addComponent(szukanie, "header");
		VerticalLayout logo_db = new VerticalLayout();
		logo_db.addStyleName("logo_db");
		
		Label left_menu = new Label();
		left_menu.addStyleName("left_menu");
		CL.addComponent(left_menu, "left_menu");
		
		CL.addComponent(logo_db, "linkowanko");
		
		VerticalLayout hl = new VerticalLayout();		
		addPersonFormBtn.addStyleName("btn_");
		editPersonFormBtn.addStyleName("btn_");
		deletePersonFormBtn.addStyleName("btn_");
		
		final FormLayout form2 = new FormLayout();
		final FieldGroup binder2 = new FieldGroup(userItem);
		
		final TextField login_login = new TextField("Login");
		binder2.bind(login_login, "login");
		login_login.addStyleName("loginMain");
		
		final PasswordField login_haslo = new PasswordField("Hasło");
		binder2.bind(login_haslo, "haslo");
		login_haslo.addStyleName("loginMain");

		
		Button zaloguj = new Button("Zaloguj");
		zaloguj.addStyleName("btn_2");
		
		if(getSession().getAttribute("user") == null) {
			form2.addComponent(login_login);
			form2.addComponent(login_haslo);
			form2.addComponent(zaloguj);
		}
		
		zaloguj.addClickListener(new ClickListener() {

			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				User user1 = new User(login_login.getValue(), login_haslo.getValue());
				userManager.addUser(user1);
				
				userManager.logIn(user1);
				//getUI().getPage().setLocation("http://www.localhost:8080/vaadindemo");
			}
		});
		
		Label menuLogowania = new Label("Logowanie do systemu");
		menuLogowania.addStyleName("menuFilmu");
		hl.addComponent(menuLogowania);		
		hl.addComponent(form2);
		
		Label menuFilmu = new Label("Twoje menu");
		menuFilmu.addStyleName("menuFilmu");
		hl.addComponent(menuFilmu);		
		
		//if(getSession().getAttribute("user") == null) {
			hl.addComponent(addPersonFormBtn);
			hl.addComponent(editPersonFormBtn);
			hl.addComponent(deletePersonFormBtn);
		//}
		Label nawigacja = new Label("Nawigacja");
		nawigacja.addStyleName("menuFilmu");
		hl.addComponent(nawigacja);			
		
		Button stronaGlowna = new Button("Strona główna");
		stronaGlowna.addStyleName("btn_3");
		hl.addComponent(stronaGlowna);

		stronaGlowna.addClickListener(new ClickListener() {

			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				getUI().getPage().setLocation("http://www.localhost:8080/vaadindemo");
			}
		});
		
		Button tabela = new Button("Tabela");
		tabela.setStyleName("btn_3");
		hl.addComponent(tabela);

		tabela.addClickListener(new ClickListener() {

			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				getUI().getPage().setLocation("http://www.localhost:8080/vaadindemo/table");
			}
		});
		
		Button przegladajFilmy = new Button("Przeglądaj Filmy");
		przegladajFilmy.addStyleName("btn_3");
		hl.addComponent(przegladajFilmy);

		przegladajFilmy.addClickListener(new ClickListener() {

			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				getUI().getPage().setLocation("http://www.localhost:8080/vaadindemo/filmy");
			}
		});
		
		Button oNas = new Button("Kilka słów o projekcie");
		oNas.addStyleName("btn_3");
		hl.addComponent(oNas);

		oNas.addClickListener(new ClickListener() {

			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				getUI().getPage().setLocation("http://www.localhost:8080/vaadindemo/onas");
			}
		});
		
		final Table filmsTable = new Table("", films);
		filmsTable.setColumnHeader("Tytuł", "tytul");
		filmsTable.setColumnHeader("Opis", "opis");
		filmsTable.setColumnHeader("Data", "czasNadawania");
		filmsTable.setColumnHeader("Reżyser", "rezyser");
		filmsTable.setColumnHeader("Kanał", "kanal");
		filmsTable.setColumnHeader("Kategoria", "kategoria");
		filmsTable.setColumnHeader("18+", "plus18");
		//filmsTable.setColumnWidth("czasNadawania", 170);
		//filmsTable.setColumnWidth("id", 50);
		//filmsTable.setColumnWidth("kanal", 100);
		//filmsTable.setColumnWidth("rezyser", 150);
		//filmsTable.setColumnWidth("kategoria", 150);
		//filmsTable.setColumnWidth("plus18", 70);
		//filmsTable.setColumnWidth("tytul", 250);
		//filmsTable.setColumnWidth("opis", 400);
		filmsTable.setSelectable(true);
		filmsTable.setImmediate(true);
		
		filmsTable.addValueChangeListener(new Property.ValueChangeListener() {

			private static final long serialVersionUID = 1L;

			@Override
			public void valueChange(ValueChangeEvent event) {

				Film selectedFilm = (Film) filmsTable.getValue();
				if (selectedFilm == null) {
					film.setTytul("");
					film.setOpis("");
					film.setCzasNadawania(null);
					film.setRezyser("");
					film.setKanal("");
					film.setKategoria("");
					film.setPlus18(false);
					film.setId(null);
				} else {
					film.setTytul(selectedFilm.getTytul());
					film.setOpis(selectedFilm.getOpis());
					film.setCzasNadawania(selectedFilm.getCzasNadawania());
					film.setRezyser(selectedFilm.getRezyser());
					film.setKanal(selectedFilm.getKanal());
					film.setKategoria(selectedFilm.getKategoria());
					film.setPlus18(selectedFilm.isPlus18());
					film.setId(selectedFilm.getId());
				}
			}
		});
		hl.addComponent(filmsTable);
        CL.addComponent(hl, "left_menu");
	}

}